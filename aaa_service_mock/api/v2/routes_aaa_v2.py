import json
from http.client import HTTPException

from fastapi.responses import JSONResponse

from ...singletons import ORGANIZATIONS_V2_BY_ORGANIZATION_ID, api_integration, organizations_v2_file_path


def add_routes_aaa_v2(app):
    @app.post(
        "/api/v2/custom-mock/organization",
        tags=["aaa-v2-custom-mock"],
    )
    async def _(
        organization: dict,
        _=api_integration.global_depends(),
    ):
        if "organization_id" not in organization:
            raise HTTPException(status_code=422, detail="[organization_id] is missing")
        ORGANIZATIONS_V2_BY_ORGANIZATION_ID[str(organization["organization_id"])] = organization
        with organizations_v2_file_path.open("w") as f:
            json.dump(ORGANIZATIONS_V2_BY_ORGANIZATION_ID, f)
        return

    @app.get(
        "/api/v2/public/organizations/{organization_id}",
        tags=["aaa-v2-public-organization-controller"],
    )
    async def _(
        organization_id: str,
        _=api_integration.global_depends(),
    ):
        if organization_id in ORGANIZATIONS_V2_BY_ORGANIZATION_ID:
            return ORGANIZATIONS_V2_BY_ORGANIZATION_ID[organization_id]
        else:
            return JSONResponse(status_code=404, content={"detail": "organization does not exist"})

    @app.get(
        "/api/v2/public/organizations",
        tags=["aaa-v2-public-organization-controller"],
    )
    async def _(
        _=api_integration.global_depends(),
    ):
        organizations = [ORGANIZATIONS_V2_BY_ORGANIZATION_ID[org_id] for org_id in ORGANIZATIONS_V2_BY_ORGANIZATION_ID]
        return {
            "total_organizations_count": len(ORGANIZATIONS_V2_BY_ORGANIZATION_ID),
            "organizations": organizations,
        }
