from .routes_server import add_routes_server
from .v2 import add_routes_v2


def add_routes(app):
    add_routes_server(app)
    add_routes_v2(app)
