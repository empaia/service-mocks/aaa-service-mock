import json
from logging import DEBUG, getLogger
from pathlib import Path

from .api_integrations import get_api_integration
from .settings import Settings

logger = getLogger("uvicorn")
settings = Settings()

if settings.debug:
    logger.level = DEBUG

api_integration = get_api_integration(settings, logger)

organizations_v2_file_path = Path(settings.storage_dir_path, "orgas_v2.json")

ORGANIZATIONS_V2_BY_ORGANIZATION_ID = {}

try:
    with organizations_v2_file_path.open() as f:
        ORGANIZATIONS_V2_BY_ORGANIZATION_ID = json.load(f)
except FileNotFoundError:
    pass
