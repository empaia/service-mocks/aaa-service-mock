from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from . import __version__ as version
from .api import add_routes
from .singletons import settings

openapi_url = "/openapi.json"
if settings.disable_openapi:
    openapi_url = ""

app = FastAPI(
    debug=settings.debug,
    title="AAA Service Mock API",
    version=version,
    description="AAA Service Mock API",
    root_path=settings.root_path,
    openapi_url=openapi_url,
)
add_routes(app)

if settings.cors_allow_origins:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=settings.cors_allow_origins,
        allow_credentials=settings.cors_allow_credentials,
        allow_methods=["*"],
        allow_headers=["*"],
    )
