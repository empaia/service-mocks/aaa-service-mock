# Changelog

## 0.2.1 - 17

* renovate

## 0.2.0

* migrate pydantic v2

## 0.1.47 & 48

* renovate

## 0.1.46

* renovate

## 0.1.45

* renovate

## 0.1.44

* renovate

## 0.1.43

* fix fastapi related bug due to breaking change

## 0.1.42

* renovate

## 0.1.41

* renovate

## 0.1.40

* renamed `keycloak_id` to `organization_id`
* removed v1 API

## 0.1.39

* bugfix for 0.1.38

## 0.1.38

* added v2 routes to obtain organization data

## 0.1.37

* renovate

## 0.1.36

* renovate

## 0.1.35

* renovate

## 0.1.34

* renovate

## 0.1.33

* renovate

## 0.1.32

* renovate

## 0.1.31

* renovate

## 0.1.30

* renovate

## 0.1.29

* renovate

## 0.1.28

* renovate

## 0.1.27

* renovate

## 0.1.26

* renovate

## 0.1.25

* renovate

## 0.1.24

* renovate

## 0.1.23

* renovate

## 0.1.22

* renovate

## 0.1.21

* renovate

## 0.1.20

* added route to get orga by keycloak_id

## 0.1.19

* renovate

## 0.1.18

* renovate

## 0.1.17

* renovate

## 0.1.16

* renovate

## 0.1.15

* renovate

## 0.1.14

* renovate

## 0.1.13

* renovate

## 0.1.12

* renovate

## 0.1.11

* renovate

## 0.1.10

* renovate

## 0.1.9

* renovate

## 0.1.8

* renovate

## 0.1.7

* renovate

## 0.1.6

* updated dependencies

## 0.1.5

* updated dependencies

## 0.1.4

* updated fastapi

## 0.1.3

* updated ci

## 0.1.2

* added cors_allow_origins to settings again and made allow_credentials also configurable via cors_allow_credentials

## 0.1.1

* fixed typo in POST organization route

## 0.1.0

* init
